/*
  Node JS Code Sample:
  
  This code sample features two authentication strategies using PassportJS
  Local Login: Standard email, password
  JWT (JSON Web Tokens) Login: Login via tokens
*/

const passport = require('passport');
const User = require('../models/user');
const config = require('../config');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const LocalStrategy = require('passport-local');

// Create local strategy
const localLogin = new LocalStrategy({ usernameField: 'email' }, function(email,password,done) {
  // Verify this email and password, call done wit the user
  // if not correct email and password
  // otherwise, call done with false
  User.findOne({ email: email }, function(err, user) {
    if (err) { return done(err); }
    if (!user) { return done(null, false); }
    
    // compare passwords
    user.comparePassword(password, function(err, isMatch) {
      if (err) { return done(err); }
      if (!isMatch) { return done(null, false); }
      
      return done(null, user);  
    });
  })
});

// Setup options for JWT Startegy
const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: config.secret
};

// Create JWT Strategy
const jwtLogin = new JwtStrategy(jwtOptions, function(payload, done) {
  // See if the user ID in the payload exists in DB
  // If it does call 'done' with object
  // otherwise call doen without user object
  User.findById(payload.sub, function(err, user) {
    if (err) { return done(err, fasle); }
    
    if(user) {
      done(null, user);
    } else {
      done(null, falase);
    }
  });
});

// Tell passport to use this strategy
passport.use(jwtLogin);
passport.use(localLogin);