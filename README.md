# Sample User Token Based Authentication
This sample server uses token-based authentication and features a basic example
for login, register, and authenticated requests

## Dependencies

* Requires MongoDB

## Directions:
* Create a config.js file in root of directory
* Add the following code:
 `module.exports = {
    secret: <YOUR SECRET KEY>
  }`
  
More information can be added below...